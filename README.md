# bitwareServices

Desarrollar API REST con JSON que permita consumir dar de alta, consultar y modificar un cliente. Además que a través de
tópicos de kafka el consumidor almacene la información en base de datos de MongoDB .
Las respuesta de la API para cada petición deberá considerar los siguientes campos base dentro de la respuesta JSON:
Cve_Error (donde 0 es exitoso y cual error es con un valor negativo).
Cve_Mensaje (la descripción del código de mensaje).

## Requeriemientos 
 java  11 
 Docker-Desktop

## Iniciar Prueba

```
cd bitwareservices
git pull
docker-compose up -d 

```
